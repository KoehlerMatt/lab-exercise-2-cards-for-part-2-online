// struct for card games

#include <iostream>
#include <conio.h>

enum Rank // ace high
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADE, DIAMOND, CLUB, HEART
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card); //Print Card in Plain Text
Card HighCard(Card card1, Card card2); //Determine High Card

int main()
{
	// testing

	Card card1;
	card1.rank = SIX;
	card1.suit = HEART;

	Card card2;
	card2.rank = QUEEN;
	card2.suit = SPADE;

	PrintCard(card1);
	PrintCard(card2);
	HighCard(card1, card2);

	_getch();
	return 0;
}

void PrintCard(Card card) //Print Card in Plain Text
{

	std::cout << "The ";
	
	//Changes the rank to Plan Text and outputs
	switch (card.rank)
	{
	case 2:
		std::cout << "Two";
		break;
	case 3:
		std::cout << "Three";
		break;
	case 4:
		std::cout << "Four";
		break;
	case 5:
		std::cout << "Five";
		break;
	case 6:
		std::cout << "Six";
		break;
	case 7:
		std::cout << "Seven";
		break;
	case 8:
		std::cout << "Eight";
		break;
	case 9:
		std::cout << "Nine";
		break;
	case 10:
		std::cout << "Ten";
		break;
	case 11:
		std::cout << "Jack";
		break;
	case 12:
		std::cout << "Queen";
		break;
	case 13:
		std::cout << "King";
		break;
	case 14:
		std::cout << "Ace";
		break;
	default:
		break;
	}
		
	std::cout << " of ";
	
	//Changes the suit to Plan Text and outputs
	switch (card.suit)
	{
	case 0:
		std::cout << "Spades.\n";
		break;
	case 1:
		std::cout << "Diamonds.\n";
		break;
	case 2:
		std::cout << "Clubs.\n";
		break;
	case 3:
		std::cout << "Hearts.\n";
		break;
	default:
		break;
	}
}

Card HighCard(Card card1, Card card2) //Determine High Card
{
	if (card1.rank > card2.rank)
	{
		std::cout << "Card 1 is higher.\n";
		return card1;
	}
	else
	{
		std::cout << "Card 2 is higher.\n";
		return card2;
	}
}